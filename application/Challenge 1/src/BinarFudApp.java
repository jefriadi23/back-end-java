import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BinarFudApp {
    private static final String MENU_FILE = "menu.txt";
    private static final String ORDER_FILE = "order.txt";
    private static final String RECEIPT_FILE = "receipt.txt";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<MenuItem> menu = loadMenu();
        ArrayList<OrderItem> order = new ArrayList<>();

        while (true) {
            System.out.println("==========================");
            System.out.println("Selamat datang di BinarFud");
            System.out.println("==========================");
            System.out.println("Silahkan pilih makanan :");
            displayMenu(menu);
            System.out.println("99. Pesan dan Bayar");
            System.out.println("0. Keluar aplikasi");
            System.out.print("=> ");

            int choice = scanner.nextInt();

            if (choice == 99) {
                if (!order.isEmpty()) {
                    // Menampilkan layar konfirmasi dan pembayaran
                    System.out.println("==========================");
                    System.out.println("Konfirmasi dan Pembayaran");
                    System.out.println("==========================");
                    displayOrder(order);
                    double total = calculateTotal(order);
                    System.out.println("Total Bayar: " + total + " RP");
                    System.out.println("1. Konfirmasi Pesanan");
                    System.out.println("2. Kembali ke Menu Utama");
                    System.out.println("0. Keluar aplikasi");
                    System.out.print("=> ");

                    int paymentChoice = scanner.nextInt();
                    switch (paymentChoice) {
                        case 1:
                            // Lakukan proses pembayaran (misalnya, cetak struk pembayaran)
                            printReceipt(order, total);

                            // Simpan struk ke file
                            saveReceipt(order, total);

                            System.out.println("Terima kasih! Pesanan Anda telah dikonfirmasi.");
                            order.clear(); // Hapus pesanan setelah konfirmasi
                            break;
                        case 2:
                            // Kembali ke menu utama
                            break;
                        case 0:
                            // Keluar aplikasi
                            System.out.println("Terima kasih! Selamat tinggal.");
                            saveOrder(order); // Simpan pesanan sebelum keluar
                            return;
                        default:
                            System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                            break;
                    }
                } else {
                    System.out.println("Anda belum memesan apa-apa.");
                }
            } else if (choice == 0) {
                System.out.println("Terima kasih! Selamat tinggal.");
                saveOrder(order); // Simpan pesanan sebelum keluar
                break;
            } else if (choice >= 1 && choice <= menu.size()) {
                MenuItem selectedItem = menu.get(choice - 1);
                System.out.print("Jumlah pesanan " + selectedItem.getName() + ": ");
                int quantity = scanner.nextInt();
                if (quantity > 0) {
                    order.add(new OrderItem(selectedItem, quantity));
                    System.out.println("Pesanan Anda telah ditambahkan.");
                } else {
                    System.out.println("Jumlah pesanan harus lebih dari 0.");
                }
            } else {
                System.out.println("Pilihan tidak valid. Silakan coba lagi.");
            }
        }
    }

    private static ArrayList<MenuItem> loadMenu() {
        ArrayList<MenuItem> menu = new ArrayList<>();
        menu.add(new MenuItem("Nasi Goreng", 15000));
        menu.add(new MenuItem("Mie Goreng", 13000));
        menu.add(new MenuItem("Nasi + Ayam", 18000));
        menu.add(new MenuItem("Es Teh Manis", 3000));
        menu.add(new MenuItem("Es Jeruk", 5000));
        return menu;
    }

    private static void displayMenu(ArrayList<MenuItem> menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.get(i);
            System.out.println((i + 1) + ". " + item.getName() + " | " + item.getPrice() + " RP");
        }
    }

    private static void displayOrder(ArrayList<OrderItem> order) {
        System.out.println("===== Pesanan Anda =====");
        for (OrderItem item : order) {
            System.out.println(item.getMenuItem().getName() + " | " + item.getQuantity() + " pcs | " + (item.getQuantity() * item.getMenuItem().getPrice()) + " RP");
        }
        System.out.println("=======================");
    }

    private static double calculateTotal(ArrayList<OrderItem> order) {
        double total = 0;
        for (OrderItem item : order) {
            total += item.getQuantity() * item.getMenuItem().getPrice();
        }
        return total;
    }

    private static void saveOrder(ArrayList<OrderItem> order) {
        System.out.println("===== Pesanan Tersimpan =====");
        for (OrderItem item : order) {
            System.out.println(item.getMenuItem().getName() + " | " + item.getQuantity() + " pcs");
        }
        System.out.println("==============================");
    }

    private static void printReceipt(ArrayList<OrderItem> order, double total) {
        System.out.println("===== Struk Pembayaran =====");
        for (OrderItem item : order) {
            System.out.println(item.getMenuItem().getName() + " | " + item.getQuantity() + " pcs | " + (item.getQuantity() * item.getMenuItem().getPrice()) + " RP");
        }
        System.out.println("Total Bayar: " + total + " RP");
        System.out.println("===========================");
    }

    private static void saveReceipt(ArrayList<OrderItem> order, double total) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(RECEIPT_FILE))) {
            writer.write("===== Struk Pembayaran =====");
            writer.newLine();
            for (OrderItem item : order) {
                writer.write(item.getMenuItem().getName() + " | " + item.getQuantity() + " pcs | " + (item.getQuantity() * item.getMenuItem().getPrice()) + " RP");
                writer.newLine();
            }
            writer.write("Total Bayar: " + total + " RP");
            writer.newLine();
            writer.write("===========================");
            System.out.println("Struk pembayaran telah disimpan sebagai " + RECEIPT_FILE);
        } catch (IOException e) {
            System.out.println("Gagal menyimpan struk pembayaran: " + e.getMessage());
        }
    }
}


class MenuItem {
    private String name;
    private double price;

    public MenuItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

class OrderItem {
    private MenuItem menuItem;
    private int quantity;

    public OrderItem(MenuItem menuItem, int quantity) {
        this.menuItem = menuItem;
        this.quantity = quantity;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public int getQuantity() {
        return quantity;
    }
}
