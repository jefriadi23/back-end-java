package application.chapter1.sesi6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileExample {
    public static void main(String[] args) {
        // Nama file yang akan kita gunakan
        String fileName = "example.txt";

        // Membuat file teks
        createTextFile(fileName);

        // Menulis ke file teks
        writeTextToFile(fileName, "Ini adalah baris pertama.\nIni adalah baris kedua.");

        // Membaca file teks dan mencetak isinya
        String content = readTextFromFile(fileName);
        System.out.println("Isi file teks:");
        System.out.println(content);
    }

    // Membuat file teks baru jika belum ada
    public static void createTextFile(String fileName) {
        try {
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("File " + fileName + " berhasil dibuat.");
            } else {
                System.out.println("File " + fileName + " sudah ada.");
            }
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan saat menciptakan file: " + e.getMessage());
        }
    }

    // Menulis teks ke dalam file
    public static void writeTextToFile(String fileName, String textToWrite) {
        try {
            FileWriter writer = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(textToWrite);
            bufferedWriter.close();
            System.out.println("Teks berhasil ditulis ke dalam file.");
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan saat menulis ke file: " + e.getMessage());
        }
    }

    // Membaca teks dari file
    public static String readTextFromFile(String fileName) {
        StringBuilder content = new StringBuilder(); // ditampung sementarai
        try {
            FileReader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content .append(line).append("\n"); // \n turn bars , \t -> tab
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan saat membaca file: " + e.getMessage());
        }
        return content.toString();
    }
}
