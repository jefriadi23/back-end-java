package application.chapter1.sesi6;

public class ContohStrin {
    public static void main(String[] args) {
        String text ="1213rffdvsdhbvskhjvnalrv7" +
                "8dvvdfb" +
                "dfvdkfkvjkdjfvd" +
                "";

        String text2 ="yuyydvbhjdfhvbdfmvbnm";

        String nama = "buah";
        String hobby= "main";
        Integer umur = 10;
        String satuan = "tahun";
        System.out.println(nama.concat(hobby));
        System.out.println(nama + hobby);
        System.out.println(satuan + umur);
//        System.out.println(satuan.concat(umur));
        cobaStringBuffer();
    }

    public static  void cobaStringBuffer(){
        StringBuilder sb = new StringBuilder();
        sb.append("Java");
        sb.append("Developer");

        String str = sb.toString();
        System.out.println("str =" + str);
    }

}
